package com.company;

/**
 Yorkshire.java
 A class derived from Dog that holds information about
 a Yorkshire terrier. Overrides Dog speak method.
 @ TODO this file is largely incomplete

 */

public class Yorkshire extends Dog
{
    private String color;
    private static int breedWeight = 25;

    public Yorkshire(String name, String color)
    {
        super(name);
        this.color = color;
    }

    /**
     * Small bark -- overrides speak method in Dog
     * @return a small bask string
     */

    public String speak()
    {
        return "woof";
    }

    public static int avgBreedWeight()
    {
        return breedWeight;
    }

    @Override
    public String toString() {
        return ", he is " + color + ", his weight is " + avgBreedWeight() + " kg, ";
    }
}

