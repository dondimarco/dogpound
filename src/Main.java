package com.company;
import java.util.Scanner;

public class Main {

    public static void main(String[] args)
    {

        Dog dog = new Dog("Spike");
        System.out.println(dog.getName() + ", says " + dog.speak());

        System.out.println();

        Labrador labrador = new Labrador("Eugenio", "black");
        System.out.println(labrador.getName() + ", " + labrador.toString() + ", says " + labrador.speak());

        System.out.println();

        Yorkshire yorkshire = new Yorkshire("Ziad", "chocolate");
        System.out.println(yorkshire.getName() + ", says " + yorkshire.speak() + yorkshire.toString());

        System.out.println();

        Cage cage = new Cage();
        cage.add(dog);
        cage.add(labrador);
        cage.add(yorkshire);
        System.out.println( cage);
        cage.chaos();
        cage.remove("Eugenio");
        System.out.println(cage);






    }
}

